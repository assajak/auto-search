import { server } from "./../../../config/general.config";

export const debounce = (fn, delay) => {
  let timer = null;
  return function () {
    const context = this,
      args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      fn.apply(context, args);
    }, delay);
  };
};

export const query = (path, params) => {
  const { host, port } = server;
  return fetch(
    `${host}:${port}${path}?${new URLSearchParams(params).toString()}`
  ).then((response) => {
    return response.json();
  });
};

export const kitcut = (text, limit) => {
  text = text.trim();
  if (text.length <= limit) {
    return text;
  }

  text = text.slice(0, limit);

  return text.trim() + "...";
};
