/**
 * This file will hold the Menu that lives at the top of the Page, this is all rendered using a React Component...
 *
 */
import React from "react";
import { debounce, query } from "../utils/utils.js";
import SearchContainer from "./SearchContainer";

class Menu extends React.Component {
  /**
   * Main constructor for the Menu Class
   * @memberof Menu
   */
  constructor() {
    super();
    this.state = {
      showingSearch: false,
      foundItems: [],
    };

    this.onSearch = debounce(this.onSearch, 300);
  }

  /**
   * Shows or hides the search container
   * @memberof Menu
   * @param e [Object] - the event from a click handler
   */
  showSearchContainer = (e) => {
    e.preventDefault();
    this.setState({
      showingSearch: !this.state.showingSearch,
      foundItems: [],
    });
  };

  /**
   * Calls upon search change
   * @memberof Menu
   * @param e [Object] - the event from a text change handler
   */
  onSearch = (e) => {
    query("/", { criteria: e }).then((foundItems) => {
      this.setState({ foundItems });
    });
  };

  /**
   * Renders the default app in the window, we have assigned this to an element called root.
   *
   * @returns JSX
   * @memberof App
   */
  render() {
    const { foundItems, showingSearch } = this.state;
    return (
      <header className="menu">
        <div className="menu-container">
          <div className="menu-holder">
            <h1>ELC</h1>
            <nav>
              <a href="#" className="nav-item">
                HOLIDAY
              </a>
              <a href="#" className="nav-item">
                WHAT'S NEW
              </a>
              <a href="#" className="nav-item">
                PRODUCTS
              </a>
              <a href="#" className="nav-item">
                BESTSELLERS
              </a>
              <a href="#" className="nav-item">
                GOODBYES
              </a>
              <a href="#" className="nav-item">
                STORES
              </a>
              <a href="#" className="nav-item">
                INSPIRATION
              </a>

              <a href="#" onClick={this.showSearchContainer}>
                <i className="material-icons search">search</i>
              </a>
            </nav>
          </div>
        </div>
        <div className={(showingSearch ? "showing " : "") + "search-container"}>
          <input
            type="text"
            onChange={(event) => this.onSearch(event.target.value)}
          />
          <a href="#" onClick={this.showSearchContainer}>
            <i className="material-icons close">close</i>
          </a>

          <SearchContainer items={foundItems} />
        </div>
      </header>
    );
  }
}

// Export out the React Component
module.exports = Menu;
