import React from "react";
import { kitcut } from "../utils/utils";

export default class SearchItem extends React.Component {
  /**
   * Main constructor for the Menu Class
   * @memberof Menu
   */
  constructor() {
    super();
  }

  render() {
    const { item } = this.props;
    return (
      <a key={item.name} className="menu-item" href="#">
        <div className="image-wrapper">
          <img src={item.picture} />
        </div>
        <div className="text-wrapper">
          <h3>{item.name}</h3>
          <span>{kitcut(item.about, 30)}</span>
        </div>
      </a>
    );
  }
}
