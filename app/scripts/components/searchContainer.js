import React from "react";
import SearchItem from "./searchItem";

export default class SearchContainer extends React.Component {
  /**
   * Main constructor for the Menu Class
   * @memberof Menu
   */
  constructor() {
    super();
  }

  renderItems(items) {
    return items.map((i) => (
      <SearchItem key={`search-item-${i._id}`} item={i} />
    ));
  }

  renderEmptyItem() {
    return <h3 className="nothing-found">Nothing found</h3>;
  }

  renderList(items) {
    if (items && items.length) {
      return this.renderItems(items);
    }

    return this.renderEmptyItem();
  }

  render() {
    const { items } = this.props;
    return <div className="menu-items">{this.renderList(items)}</div>;
  }
}
